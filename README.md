# browsers.js
JavaScript code to recognize the web browser

- possible output strings of the function `getCurrentBrowser()`:
  - **strings**: safari, firefox, chrome, internet-explorer, edge, opera, opera-mini, undefined
  
Run: [https://rawgit.com/pesout/browsers.js/master/test.html](https://rawgit.com/pesout/browsers.js/master/test.html)
